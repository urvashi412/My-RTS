﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour {

	public static MouseManager current;
	List<Interactive> selections=new List<Interactive>();
	public MouseManager(){
		current=this;
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(!Input.GetMouseButtonDown(0))
			return;
		var ray=Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(!Physics.Raycast(ray,out hit))
			return;
		Interactive interactive=hit.transform.GetComponent<Interactive>();
		if(interactive==null) return;
		interactive.Select();
	}
}
