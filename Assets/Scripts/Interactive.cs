﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactive : MonoBehaviour {

	private bool _selected=false;
	public bool swap=false;
	public bool Selected {
		get{return _selected;}
	}
	void Update(){
		if(swap==true){
			swap=false;
			if(_selected) Deselect();
			else
				Select();
		}
	}
	public void Select(){
		_selected=true;
		Interaction[] interactions=GetComponents<Interaction>();
		foreach(Interaction interaction in interactions){
			interaction.Select();
		}
	}
	public void Deselect(){
		_selected=false;
		Interaction[]  interactions=GetComponents<Interaction>();
		foreach(Interaction interaction in interactions){
			interaction.Deselect();
		}
	}

}
